
COMP 410

List ADT (Implemented with Linked Cells)


Overview
You are going to build in Java a general List ADT (LIST) using linked cells to do it. It will also me modifed over the traditional behavior to include an operation that would assist in sorting.

Using linked cells means do not use arrays to contain the elements stored in the List. It also means do not use any of the code from the Java Collections library to get your work done.

The LIST you will implement will be very similar to the ADT shown in your text. It will have an operation to put an element into the list by position, to take an element out by position, and to retrieve an element by position (without altering the data structure). It will have an operation telling how many elements are stored in the list, and a boolean to tell if it is empty or not. It has an operation to make a fresh, empty list. We have left out the operations shown in the text that search for elements by value.

It will also have an operation called "insort" that will insert an element into the list in a way that will support sorting. The specific details of insort behavior is given in the specs below.

The class Node below shows how we do linked cells in Java. A linked cell is an object (class instance) that has one (or more) references to other objects of the same class. So a list cell refers to another list cell (the next one in the linked chain).

Our implementation will be a doubly linked list. As you see in the Node class, there is a next field and a prev field. The next field points to the cell that follows the current cell in the list. The prev field points to the cell that came just before the current one in the list. To go through the list item by item in the "forward" direction you would start with the first cell and use the next field until you get to the end of the list. Since it is doubly linked, you can also traverse the list "backwards" by going to the last cell and following the prev links until you get to the first cell. You will need a pointer as well in your List object that points to "end" just as you have a link that points to "head".



We ask that you use the boilerplate code provided and work out of it. You should copy and paste in the boilerplate below and structure it as we described.

Project,package,zip names matter!! It's how we grade your code! A good reference if you get stuck is your text book (pg 75-82).

What to Hand-in
Hand in your code via Sakai, and follow the instructions there for how to remove various parts and zip it all up.

Interfaces
For uniformity, to aid grading, you must write your program with these specific structures.

Interface LIST_Interface
/**
 * COMP 410
 *
 * Make your class and its methods public!
 * Don't modify this file!
 *
 * Your LinkedList implementation will implement this interface.
 *
*/


package LinkedList_A1;
/*
  Interface: The LIST will provide this collection of operations:

  clear:
    in: nothing
    return: void
    effect: list is left with size 0, no elements in it,
            consists of just the original root Node
  size:
    in: nothing
    return: number of elements stored in the list
    effect: no change to list state
  isEmpty:
    in: nothing
    return: boolean, true if the list has no elements in it, true is size is 0
    effect: no change to list state
  insert
    in: a double (the data element), and an int (position index)
    return: boolean, return true if insert is successful, false otherwise
    effect: the list state will be altered so that the element is located at the
            specified index; the list has size bigger by one; all elements that were
            at the specified index or after have been moved down one slot
    error: if index is beyond list size range return false
           valid inserts take place either at a location where a list element
           already exists, or at the location that is one beyond the last element
  remove
    in: an int (the index of the element to take out of the list)
    return: boolean.. return true if the remove is successful, false otherwise
    effect: list state is altered in that the Node at the specified index is decoupled
            list size decreases by one
    errors: what if specified index is not in the list? return false
  get
    in: an int, the index of the element item to return
    return: double, the element stored at index, or Double.NaN
    effect: no change in state of the list
    error: what if index is not in the list? return Double.NaN
  insort
    in: a double (the data element)
    return: boolean, return true if insort is successful, false otherwise
    effect: the list state will be altered so that the element is located 
            at the first location where the previous element is smaller than
            or equal to the element being added (or not there), and the 
            following element is larger than the element being added 
            (or not there).  Size of list will always be increased by 1
            Another way to think of this is put te new element in between
            the first pair of elements that straddle it... with special
            cases for when the new element should be the first or last element.
    error: no errors... all insorts will succeed
*/

// ADT operations

public interface LIST_Interface {
    public boolean insert(double elt, int index);
    public boolean insort(double elt);
    public boolean remove(int index);
    public double get(int index);
    public int size();
    public boolean isEmpty();
    public void clear();
}
Class LinkedListImpl
/**
 * COMP 410
 *See inline comment descriptions for methods not described in interface.
 *
*/
package LinkedList_A1;

public class LinkedListImpl implements LIST_Interface {
  Node headCell; //this will be the entry point to your linked list (the head)
  Node lastCell; // this is the Node at the end of the list... the starting place
                 // if you wanted to traverse the list backwards
  
  public LinkedListImpl(){//this constructor is needed for testing purposes. Please don't modify!
    headCell = null; //Note that the root's data is not a true part of your data set!
    lastCell = null;
  }
  
  //implement all methods in interface, and include the getRoot method we made for testing 
  // purposes. Feel free to implement private helper methods!

 // add the fields you need to add to make it work... like a 
  
  public Node getRoot(){ //leave this method as is, used by the grader to grab your linkedList easily.
    return headCell;
  }
  public Node getLast(){ //leave this method as is, used by the grader to grab your linkedList easily.
    return lastCell;
  }
}
Class Node
/**
 * COMP 410
 * Don't modify this file!
*/

package LinkedList_A1;

public class Node { //this is your Node object, these are the Objects in your list
  public double data;
  public Node next; //links this node to the next Node in the List
  public Node prev; //links this node to the preceeding Node in the List (ie this Node is the prev Node's next node)
  public Node(double data){
    this.data=data;
    this.next=null;
    this.prev=null;
  }
  public String toString(){
    return "data: "+data+"\thasNext: "+(next!=null)+"\t\thasPrev: "+(prev!=null);
  }
  
  /*  Below are "getters" for our testing purposes. Please do not modify.  
      I would advise just referencing the fields of your Nodes since 
      they are public
  */
  public boolean isNode(){ //testing purposes please do not touch!
    return true;
  }
  public double getData(){ //testing purposes please do not touch!
    return data;
  }
  public Node getNext(){ //testing purposes please do not touch
    return next;
  }
  public Node getPrev(){ //testing purposes please do not touch
    return prev;
  }
}
Class LinkedListPlayground
package LinkedList_A1;

public class LinkedListPlayground {

  public static void main(String[] args) { 
    /*
     here you can instantiate your LinkedList and play around with it to check
     correctness. We've graciously also provided you a bit of extra test data for debugging.
     It doesn't matter what you have in here. We will not grade it. This is for your use in testing your implementation.
      */
    test1();
    test2();

  }
  
  public static void test1(){
    // example test cases
    LinkedListImpl L= new LinkedListImpl();
    System.out.println(L.isEmpty());
    printList(L);
    L.clear();
    System.out.println(L.isEmpty());
    printList(L);
    L.insert(3.3,0);
    System.out.println(L.isEmpty());
    printList(L);
    L.insert(3.4, 0);
    L.insert(3.5, 0);
    L.insert(3.67, 1);
    L.insert(3.357, 0);
    L.insert(3.333, 4);
    System.out.println(L.size());
    printList(L);
    L.remove(3);
    System.out.println(L.size());
    printList(L);
    L.clear();
    L.insert(3.4, 0);
    L.insert(3.5, 0);
    L.insert(3.67, 1);
    L.insert(3.357, 0);
    L.insert(3.333, 3);
    L.remove(0);
    System.out.println(L.size());
    printList(L);
  }

  public static void test2(){
    // example test cases
    LinkedListImpl L= new LinkedListImpl();
    L.insert(3.4,0);
    L.insert(3.5,1);
    L.insert(3.67,2);
    L.remove(0);
    System.out.println(L.size());
    printList(L);
  }
  
  public static void printList(LinkedListImpl L){ 
    //note that this is a good example of how to iterate through your linked list
    // since we know how many elements are in the list we can use a for loop
    Node curr=L.headCell; // the first data node in the list... might be null
    System.out.print("List: ");
    for(int i=0; i<L.size(); i++) { 
      System.out.print(" --> " + curr.data);
      curr=curr.next;
    }
    System.out.println();
  }
}
Class RunTests
/**
 * COMP 410
 * Don't modify this file!
 * This file is optional and is only needed to run the Oracle if
 * it is in your build path and the jar is in your project.
*/

package LinkedList_A1;
import gradingTools.comp410s20.assignment1.testcases.Assignment1Suite;

public class RunTests {
  public static void main(String[] args){
    Assignment1Suite.main(args);
  }
}

Notes on Operations and Objects
size
The size operation simply returns the count of how many elements are in the list. A brand new empty should have its numElts field be 0. Method size will return 0 if the list is empty, and always return a 0 or greater. Note the isEmpty() method is synonymous with size()==0 .

insert and remove
Note that we wish for you to look out for the edge case of inserting or removing in an index that is not reachable (ie < 0 or > size of list) We ask that you return false in these cases. Also when inserting and removing be careful to connect your nodes to the next (and prev) nodes inside the list. You don't want to get a null pointer when iterating, inserting, and removing. Remember the special case of inserting the very first data node... you will have to alter the headCell field of the list object. You will have a similar special case when removing the last data cell and changing the list from size 1 to size 0.

insort
The insort operation can be used to sort if is is the only "add" operation used... that is, if you always do insort and never use insert, then the list will always remain in sorted order.

Here are examples of how insort works; let L be a variable containing a list object:

operation        resulting list
                 
L.insort(14)     14
L.insort(21)     14 21
L.insort(5)       5 14 21
L.insort(17)      5 14 17 21
L.insort(67)      5 14 17 21 67

                 
L.insort(10)     10
L.insort(15)     10 15
L.insort(15')    10 15 15'         (a second "15" goes after the first one)
L.insort(20)     10 15 15' 20
L.insort(15'')   10 15 15' 15'' 20              
L.insort(10')    10 10' 15 15' 15'' 20
Those examples show how insort alone will give a sorted list. But it is valid to use the operations mixed. Here are examples of how insort misex with other operstions; let L be a variable containing a list object:

operation        resulting list
                 
L.insort(14)     14
L.insort(21)     14 21
L.insert(10,1)   14 10 21      (put 10 at position 1)
L.remove(2)      14 10
L.insort(8)       8 14 10      (in sort order, 8 comes before the very first item)
L.insert(20,3)    8 14 10 20
L.insort(17)      8 14 10 17 20  (17 is bigger than 14, but 10 is smaller)
                                 (insort keeps looking for the first pair of elements
                                  that is properly fits between)
L.insort(14')     8 14 10 14' 17 20


Testing and Output
For testing, use your own test cases in LinkedListPlayground class. Think in detail about that tests will cover the full range of behavior your list might go through in use, and make sure your code handles those situations correctly. We also encourage using the oracle program for incremental development. Instructions on using the oracle will be in the form of the ppt presented in class (and posted online).

There is no specific output or runnable class required for this assignment. We assume you will use the "print" capability provided that will assist you in seeing how your code is working.

For grading, we will be using a grading program that will instantiate your objects and perform the LIST ADT operations on it. We will then observe if the functionality of the data structure is as we requested. Be sure to test beyond the scope of the Oracle as mentioned in class! The order in which ADT operations are performed and what data is entering your list will be different in the official grading script. There will be behavior that is required (in the interface you are implementing) but that is not tested in the oracle we gave you.










