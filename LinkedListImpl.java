/**
 * COMP 410
 *See inline comment descriptions for methods not described in interface.
 *
*/
package LinkedList_A1;

public class LinkedListImpl implements LIST_Interface {
  Node headCell; //this will be the entry point to your linked list (the head)
  Node lastCell; // this is the Node at the end of the list... the starting place
                 // if you wanted to traverse the list backwards
  int size = 0;
  
  public LinkedListImpl(){//this constructor is needed for testing purposes. Please don't modify!
    headCell = null; //Note that the root's data is not a true part of your data set!
    lastCell = null;
  }
  
  //implement all methods in interface, and include the getRoot method we made for testing 
  // purposes. Feel free to implement private helper methods!

 // add the fields you need to add to make it work... like a 
  
  public Node getRoot(){ //leave this method as is, used by the grader to grab your linkedList easily.
    return headCell;
  }
  public Node getLast(){ //leave this method as is, used by the grader to grab your linkedList easily.
    return lastCell;
  }

//  insert
//  in: a double (the data element), and an int (position index)
//  return: boolean, return true if insert is successful, false otherwise
//  effect: the list state will be altered so that the element is located at the
//          specified index; the list has size bigger by one; all elements that were
//          at the specified index or after have been moved down one slot
//  error: if index is beyond list size range return false
//         valid inserts take place either at a location where a list element
//         already exists, or at the location that is one beyond the last element
  
  	public boolean insert(double elt, int index) {
  		Node newNode = new Node(elt);
  		
  		// Index is out of range
  		if (index > size | index < 0) {
  			return false;
  		}
  		
  		// LinkList of size 0
  		if (size == 0) {
  			headCell = newNode;
  			lastCell = newNode;
  			size++;
  			return true;
  		}
  		
  		// New headCell
  		if (index == 0) {
  			headCell.prev = newNode;
  			newNode.next = headCell;
  			headCell = newNode;
  			size++;
  			return true;
  		}
  		
  		// New lastCell
  		if (index == size) {
  			lastCell.next = newNode;
  			newNode.prev = lastCell;
  			lastCell = newNode;
  			size++;
  			return true;
  		}
  		
  		Node current = headCell;
  		int currentIndex = 0;
		while (current != null) {
			if (index == currentIndex) {
				current.prev.next = newNode;
				newNode.prev = current.prev;
				newNode.next = current;
				current.prev = newNode;
				size++;
				return true;
			}
			currentIndex++;
			if (current.next == null) {
				return false;
			}
			current = current.next;
		}
  		return false;
  	}

//	insort 
//	in: a double (the data element)
//	return: boolean, return true if insort is successful, false otherwise
//	effect: the list state will be altered so that the element is located 
//        at the first location where the previous element is smaller than
//        or equal to the element being added (or not there), and the 
//        following element is larger than the element being added 
//        (or not there).  Size of list will always be increased by 1
//        Another way to think of this is put te new element in between
//        the first pair of elements that straddle it... with special
//        cases for when the new element should be the first or last element.
//  error: no errors... all insorts will succeed

	public boolean insort(double elt) {
		Node newNode = new Node(elt);
		
		// LinkList of size 0
  		if (size == 0) {
  			headCell = newNode;
  			lastCell = newNode;
  			size++;
  			return true;
  		}
  		
  		// New headNode
  		if (newNode.data < headCell.data) {
  			headCell.prev = newNode;
  			newNode.next = headCell;
  			headCell = newNode;
  			size++;
  			return true;
  		}
  		
  		Node current = headCell;
  		while (current != null) {
  			if (current.next == null) {
  				if (newNode.data < current.data)
  				return false;
  			}
  			if (newNode.data >= current.data) {
  				if (current.next == null) {
  					current.next = newNode;
  					newNode.prev = current;
  					lastCell = newNode;
  					size++;
  					return true;
  				}	
  				if (newNode.data < current.next.data) {
  					newNode.next = current.next;
  					current.next.prev = newNode;
  					newNode.prev = current;
  					current.next = newNode;
  					size++;
  					return true;
  				}
  			}
  			current = current.next;
  		}
		return false;
	} 

//	remove
//	in: an int (the index of the element to take out of the list)
//	return: boolean.. return true if the remove is successful, false otherwise
//	effect: list state is altered in that the Node at the specified index is decoupled
//        list size decreases by one
//	errors: what if specified index is not in the list? return false

	public boolean remove(int index) {
		
		// Index out of range
		if (index > size-1 | index < 0) {
		  	return false;
		}
		
		// New headCell
		if (index == 0) {
			headCell = headCell.next;
			headCell.prev = null;
			size--;
			return true;
		}
		
		// New lastCell
		if (index == size-1) {
			lastCell = lastCell.prev;
			lastCell.next = null;
			size--;
			return true;
		}
		
		// Iterate current till index
		Node current = headCell;
		int currentIndex = 0;
		while (current != null) {
			if (index == currentIndex) {
				current.prev.next = current.next;
				current.next.prev = current.prev;
				current = null;
				size--;
				return true;
			}
			currentIndex++;
			if (current.next == null) {
				return false;
			}
			current = current.next;
		}
		return false; 
	}
 
//	get
//	in: an int, the index of the element item to return
//	return: double, the element stored at index, or Double.NaN
//	effect: no change in state of the list
//	error: what if index is not in the list? return Double.NaN
	
	public double get(int index) {
		
		// Index out of range
		if (index > size-1 | index < 0) {
  			return Double.NaN;
  		}
		
		// Iterate current till index
		Node current = headCell;
		int currentIndex = 0;
		while (current != null) {
			if (index == currentIndex) {
				return current.data;
			}
			currentIndex++;
			if (current.next == null) {
				return Double.NaN;
			}
			current = current.next;
		}
		return Double.NaN;
	}

//	size: 
//  in: nothing
//  return: number of elements stored in the list
//  effect: no change to list state

	public int size() {
		return size;
	}

//	isEmpty:
//  in: nothing
//  return: boolean, true if the list has no elements in it, true is size is 0
//  effect: no change to list state
	
	public boolean isEmpty() {
		if (size == 0) { return true; }
		return false;
	}

//	clear:
//  in: nothing
//  return: void
//  effect: list is left with size 0, no elements in it,
//          consists of just the original root Node
	
	public void clear() {
		headCell = null;
		lastCell = null;
		size = 0;
	}
}
